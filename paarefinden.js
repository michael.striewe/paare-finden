class LogEvent {
	constructor(time, element, content) {
		this.time = time;
		this.element = element;
		this.content = content;
	}
}

class SPARQLQueryDispatcher {
	constructor( endpoint ) {
		this.endpoint = endpoint;
	}

	query( sparqlQuery ) {
		const fullUrl = this.endpoint + '?query=' + encodeURIComponent( sparqlQuery );
		const headers = { 'Accept': 'application/sparql-results+json' };

		return fetch( fullUrl, { headers } ).then( body => body.json() );
	}
}

const board = document.getElementById('game-board');  
 
let data, cards;
let hasFlippedCard = false;
let lockBoard = false;
let firstCard, secondCard;
let matches = 0;
let pairs = 8;
let consentMode = 'optin';
let haveConsent = false;
let resetTimeSec = 1.5;
let elements = Array();
let log = Array();
let start;

document.onvisibilitychange = () => {
  if (document.visibilityState === "hidden" && log.length>1 && haveConsent) {
    sendLog(log, start, false);
  }
};

document.onunload = () => {
  if (log.length>0 && haveConsent) {
    sendLog(log, start, false);
  }
};

if (window.location.search) {
	loadJsonFile(window.location.search.substring(1));
} else {
	showFeaturedGames();
}

async function loadJsonFile(path) {
	const json = await fetch('games/' + path + '.json')
		.then(response => {
			if (response.ok) {
				return response.text();
			}
			throw new Error('No game with name "' + path  + '" available.');
		})
		.catch((error) => {
			return null;
		});
	
	if (json != null) {
		data = JSON.parse(json);
		initBoard();
	} else {
		showFeaturedGames();
	}
}

function showFeaturedGames() {
	document.getElementById('intro').style.display='none';
	document.getElementById('container').style.display='none';
	document.getElementById('loading-screen').style.visibility='hidden';
}

function populateBoard(cards, pairs) {
  var i = 0;
  var selectedPairs = 0;
  var seenData = Array();
	
  while (selectedPairs < pairs && i < cards.length) {
	var anythingSeen = false;
	
    if (typeof cards[i] == 'object' && cards[i].length > 1) {
	  for (var j = 0; j < cards[i].length; j++) {
		if (seenData.includes(cards[i][j])) anythingSeen = true;
	  }
	} else {
	  if (seenData.includes(cards[i])) anythingSeen = true;
	}
	
	if (!anythingSeen) {
      if (typeof cards[i] == 'object' && cards[i].length > 1) {
	    for (var j = 0; j < cards[i].length; j++) {
		  if (cards[i][j].length > 0) {
		    seenData.push(cards[i][j]);
		  }
		}
	    if (cards[i].length > 2 && data.fixfirst) {
	      index = Math.floor(Math.random() * (cards[i].length-1));
		  while (!cards[i][(index + 1)]) {
	        index = Math.floor(Math.random() * (cards[i].length-1));
		  }
          createCard(hashCode(cards[i][0]) + '-0', cards[i][0]);
          createCard(hashCode(cards[i][0]) + '-' + (index + 1), cards[i][(index + 1)]);
	    } else {
	      index = Math.floor(Math.random() * cards[i].length);
		  while (!cards[i][(index)]) {
	        index = Math.floor(Math.random() * (cards[i].length));
		  }
		  otherIndex = (index + 1) % cards[i].length;
		  while (!cards[i][(otherIndex)] || otherIndex == index) {
	        otherIndex = (otherIndex + 1) % cards[i].length;
		  }
          createCard(hashCode(cards[i][0]) + '-' + index, cards[i][index]);
          createCard(hashCode(cards[i][0]) + '-' + otherIndex, cards[i][otherIndex]);
	    }
	  } else {
        createCard(hashCode(cards[i]) + '-A', cards[i]);
        createCard(hashCode(cards[i]) + '-B', cards[i]);
		seenData.push(cards[i]);
	  }
	
	  selectedPairs++;
	}
	
	i++;
  }
}

function createCard(datakey, content) {
  if (content.toLowerCase().endsWith('.jpg') || content.toLowerCase().endsWith('.png') || content.toLowerCase().endsWith('.svg')) {
    board.innerHTML += '<div class="game-card pairs' + pairs + '" data-framework="' + datakey + '"><div class="front-face"><img src="' + content + '" /></div><div class="back-face">?</div></div>';
  } else {
    board.innerHTML += '<div class="game-card pairs' + pairs + '" data-framework="' + datakey + '"><div class="front-face">' + content + '</div><div class="back-face">?</div></div>';
  }
}

function createExample(pair, isCorrect) {
	document.getElementById('examples').innerHTML += '<div class="example">' + createExampleCard(pair[0]) + '<div>+</div>' + createExampleCard(pair[1]) + (isCorrect?'<div class="example-green">✓</div>':'<div class="example-red">✗</div></div>');
}

function createExampleCard(content) {
  if (content.endsWith('.jpg') || content.endsWith('.png') || content.endsWith('.svg')) {
    return '<div class="sample-card"><div class="front-face"><img src="' + content + '" /></div></div>';
  } else {
    return '<div class="sample-card"><div class="front-face">' + content + '</div></div>';
  }
}

function initBoard() {
	document.getElementById('featured-games').style.display='none';

	if (data.name) {
	  document.getElementById('title').innerHTML = data.name;
	  document.title = data.name + " | Paare finden";
	}

	if (data.description) {
	  document.getElementById('text').innerHTML = data.description;
	}

	if (data.credits) {
	  data.credits.forEach(credit => { document.getElementById('credits').innerHTML += '<p>' + credit + '</p>'} );
	}
	
	if (data.style && typeof data.style == 'object')  {
	  document.body.style.background = data.style[0];
	  document.body.style.color = data.style[1];
	  document.querySelector('a').style.color = data.style[1];
	  document.getElementById('start-button').style.background = data.style[0];
	  document.getElementById('restart-button').style.background = data.style[0];
	  document.querySelector("meta[name=theme-color]").setAttribute("content", data.style[0]);
	}
	
	if (data.examples) {
	  data.examples.correct.forEach(pair => createExample(pair, true));
	  data.examples.incorrect.forEach(pair => createExample(pair, false));
	} else {
	  document.getElementById('examples-head').remove();
	  document.getElementById('examples').remove();
	}

	if (data.research) {
	  consentMode = data.research;
	}
	
	if (consentMode == 'none') {
	  document.getElementById('consent').remove();
	  document.getElementById('privacy').children[2].remove();
	}
	if (consentMode == 'optout') document.getElementById('consent-box').checked = true;

	if (data.pairs && (data.pairs == 6 || data.pairs == 8 || data.pairs == 10 || data.pairs == 12 || data.pairs == 14 || data.pairs == 15)) {
		pairs = data.pairs;
	}
	
	board.classList.add('pairs' + pairs);

	if (data.resetTimeSec) {
	  resetTimeSec = data.resetTimeSec;
	}
	
	document.getElementById('start-button').addEventListener('click', startGame);
	document.getElementById('restart-button').addEventListener('click', restartGame);
	
	
	if (data.source.sparql) {
		const queryDispatcher = new SPARQLQueryDispatcher( data.source.sparql.endpointUrl );
		queryDispatcher.query( data.source.sparql.query ).then( result => { processSparqlResult(result); initGame(); } );
	} else if (data.source.static) {
	  elements = data.source.static;
	  initGame();
	} 
}

function processSparqlResult(result) {
	if (result.head.vars.length == 1) {
		result.results.bindings.forEach(entry => elements.push(entry[result.head.vars[0]].value));
	} else if (result.head.vars.length > 1) {
		for (var i = 0; i < result.results.bindings.length; i++) {
			var set = Array();
			result.head.vars.forEach(el => (result.results.bindings[i][el])?set.push(result.results.bindings[i][el].value):set.push(""));
			elements.push(set);
		}
	}
}

function initGame() {
  board.innerHTML = '';
  createdCards = Array();
  shuffleArray(elements);
  populateBoard(elements, pairs);
  cards = document.querySelectorAll('.game-card');
  cards.forEach(card => card.addEventListener('click', flipCard));
  matches = 0;

  shuffleCards();
  document.getElementById('loading-screen').style.visibility='hidden';
}

function startGame() {
  start = Date.now();
  
  haveConsent = document.getElementById('consent-box') && document.getElementById('consent-box').checked;
  document.getElementById('start-pane').style.visibility='hidden';
  document.getElementById('container').scrollIntoView();
}

function restartGame() {
  initGame();

  start = Date.now();

  document.getElementById('end-pane').style.visibility='hidden';
  document.getElementById('container').scrollIntoView();
}

function flipCard() {
  if (lockBoard) return;
  if (this === firstCard) return;

  if (haveConsent) {
    if (this.querySelector('.front-face').firstChild.src) {
		log.push(new LogEvent(Date.now()-start, this.dataset.framework, this.querySelector('.front-face').firstChild.src));
	} else {
		log.push(new LogEvent(Date.now()-start, this.dataset.framework, this.querySelector('.front-face').innerHTML));
	}
  }

  this.classList.add('flip');

  if (!hasFlippedCard) {
    hasFlippedCard = true;
    firstCard = this;

    return;
  }

  secondCard = this;
  checkForMatch();
}

function checkForMatch() {
  let isMatch = firstCard.dataset.framework.substring(0,firstCard.dataset.framework.length-2) === secondCard.dataset.framework.substring(0,secondCard.dataset.framework.length-2);

  if (isMatch) {
    firstCard.removeEventListener('click', flipCard);
    secondCard.removeEventListener('click', flipCard);
	matches++;

    setTimeout(() => {
	  firstCard.classList.add('checked');
	  secondCard.classList.add('checked');

      resetBoard();
    }, 500);
	
	if (matches == pairs) {
	  if (haveConsent) {
		sendLog(log, start, true);
		log = Array();
	  }
	  
      setTimeout(() => {
        document.getElementById('end-pane').style.visibility='visible';
	  }, 1000);
	}
  } else {
    lockBoard = true;

    setTimeout(() => {
      firstCard.classList.remove('flip');
      secondCard.classList.remove('flip');

      resetBoard();
    }, resetTimeSec*1000);
  }
}

function resetBoard() {
  [hasFlippedCard, lockBoard] = [false, false];
  [firstCard, secondCard] = [null, null];
}

function shuffleCards() {
  cards.forEach(card => {
    let randomPos = Math.floor(Math.random() * pairs * 2);
    card.style.order = randomPos;
  });
}

function shuffleArray(array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
}

function sendLog(log, startTime, isComplete) {
	let logFile = JSON.stringify(log);
	fetch('logging.php', {
	  method: 'POST',
	  headers: {
		'Accept': 'application/text',
		'Content-Type': 'application/x-www-form-urlencoded'
	  },
	  body: "game=" + window.location.search.substring(1) + "&time=" + startTime + "&isComplete=" + isComplete + "&log=" + logFile
	});
}

// cyrb53
function hashCode(str, seed = 0) {
    let h1 = 0xdeadbeef ^ seed, h2 = 0x41c6ce57 ^ seed;
    for(let i = 0, ch; i < str.length; i++) {
        ch = str.charCodeAt(i);
        h1 = Math.imul(h1 ^ ch, 2654435761);
        h2 = Math.imul(h2 ^ ch, 1597334677);
    }
    h1  = Math.imul(h1 ^ (h1 >>> 16), 2246822507);
    h1 ^= Math.imul(h2 ^ (h2 >>> 13), 3266489909);
    h2  = Math.imul(h2 ^ (h2 >>> 16), 2246822507);
    h2 ^= Math.imul(h1 ^ (h1 >>> 13), 3266489909);
  
    return 4294967296 * (2097151 & h2) + (h1 >>> 0);
};

