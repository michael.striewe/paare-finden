<?php
ini_set('display_errors', 1);
error_reporting(-1);

// If there is no game id or log, we fail
if (!isset($_POST['game']) || !isset($_POST['log'])) die;

// If log is empty, we fail
if (empty($_POST['log'])) die;

// If game id is empty, we use default
if (empty($_POST['game'])) {$game = 'default';} else {$game = $_POST['game'];}

// If game id or log is too large, we fail (spam protection)
if ((strlen($_POST['game'])>64) || strlen($_POST['log'])>1024 * 1024) die;

// If there is no game definition for the given game id, we fail
if (!file_exists('games/' . $game . '.json')) die;

// Create folders if necessary
if (!file_exists('logs')) {
	mkdir('logs', 0755, true);
}
if (!file_exists('logs/' . $game)) {
	mkdir('logs/' . $game, 0755, true);
}

// Create file name
if (isset($_POST['time']) && !empty($_POST['time'])) {
	$time = $_POST['time'];
} else {
	$time = time();
}
$fileName = 'logs/' . $game . '/' . $time . '.json';
$fileNamePartial = 'logs/' . $game . '/partial' . $time . '.json';

// Delete older (partial) log file if exists
if (file_exists($fileNamePartial)) {
	unlink($fileNamePartial);
}

// Store log file
$fp = fopen((isset($_POST['isComplete'])&&$_POST['isComplete']=='true')?$fileName:$fileNamePartial, "w");
fwrite($fp, $_POST['log']);
fclose($fp);

echo "OK";

?>