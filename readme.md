## Installation

Alle Dateien aus dem Hauptordner sowie den kompletten Unterordner `/games` an der gewünschten Stelle ablegen.

Im Installationsordner einen leeren Ordner `/logs` anlegen und mit Schreibrechten versehen.

In `index.html` die Betreiberinformationen vervollständigen und nach Belieben vorgeschlagene Spiele ergänzen.

### Sicherheit

Es ist empfehlenswert, sowohl den Hauptordner als auch den Unterordner `/logs` auf geeignete Weise mit `.htaccess`-Dateien zu schützen.

Für den `/logs`-Ordner sollte mindesten `Options -Indexes` gesetzt werden.

Für den Hauptordner ist mindestens folgende Einstellung sinnvoll:

```
Options -Indexes

AuthType Basic
AuthName "Paare finden Administration"
AuthUserFile /pfad/zur/.htpasswd
Require all  granted

<Files analytics.php>
Require user  adminusername
</Files>
```

## Konfiguration

Alle Spielvarianten sind jeweils als json-Datei im Ordner `/games` abgelegt. Der Dateiname entspricht dem Parameter, der an die URL angehangen werden muss. Fehlt der Parameter, wird `default.json` verwendet. Neue Spielvarianten können durch Anlegen einer entsprechenden json-Datei erzeugt werden.

In der json-Datei können folgende Einstellungen gemacht werden:
* name: Titel des Spiels. Wird auf der Seite und in der Titelleiste des Browsers angezeigt.
* description: Beschreibung des Spiels. Wird auf der Seite angezeigt.
* credits: Hinweise zur Urheberschaft. Mehrere Strings durch Komma getrennt möglich. Werden auf der Seite im Footer hinzugefügt.
* research: Steuert die Datenerhebung. Es gibt drei Optionen:
  * "none" = kein Logging;
  * "optin" = Logging bei aktiver Zustimmung (default);
  * "optout" = Logging immer wenn nicht aktiv abgelehnt wurde.
* style: RGB-Codes für Hintergrundfarbe und Schriftfarbe der Seite.
* pairs: Zahl der verwendeten Kartenpaare. Gültige Werte sind 6, 8, 10, 12, 14 und 15. Default ist 8.
* resetTimeSec: Wartezeit in Sekunden, nach der ein falsches Kartenpaar wieder verdeckt wird. Default ist 1.5.
* fixfirst: Wenn Kartenpaare zufällig aus einer größeren Menge zusammengestellt werden (s.u.), kann mit "true" dafür gesorgt werden, dass nur die zweite Karte eines Paares variiert und die erste immer dem ersten Eintrag in der Liste entspricht.
* source: Quelle der Kartenpaare. Es gibt zwei Optionen:
  * "static" = Liste von festen Einträgen, wobei jeder Eintrag entweder ein einzelner String ist (Paar aus zwei identischen Karten) oder eine Liste von zwei Strings (Paar aus zueinandergehörigen Karten) oder eine Liste von mehr als zwei Strings (Paar aus zufälig gewählten zwei zueinandergehörigen Karten; siehe dazu oben die Option "fixfirst");
  * "sparql" = Benötigt die beiden Parameter "endpointURL" (URL gegen die eine SPARQL-Query gerichtet werden kann) und "query" (SPARQL-Query, die Datensätze mit einer, zwei oder mehr Spalten zurückgibt. 
  * In beiden Fällen werden für die Erstellung der Karten Strings, die auf ".jpg", ".png" oder ".svg" enden als URLs zum Laden eines Bildes behandelt.
* examples: Illustrative Beispiele für richtige und falsche Paare. Werden unter der Beschreibung ergänzt. Es gibt zwei Parameter:
  * "correct" = Liste von Listen mit je zwei Strings (dürfen identisch sein) als Beispiele für richtige Paare.
  * "incorrect" = Liste von Listen mit je zwei Strings als Beispiele für falsche Paare.

## Aufruf

Durch den Aufruf von `index.html` via Webbrowser wird die Startseite aufgerufen.

Durch den Aufruf von `index.html?name-des-spiels` können die Spiele aufgerufen werden, die jeweils in `/games/name-des-spiels.json` definiert sein müssen.

Für das Analytics-Dashboard gilt dasselbe, nur mit `analytics.php` als Dateiname.

## Screenshots

![Spielfeld](doku/board1.png)

![Analyse-Dashboard](doku/analytics1.png)