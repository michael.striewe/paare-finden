<html>
	<head>
		<title>Paare finden | Analytics</title>
		
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		
		<link rel="stylesheet" type="text/css" href="analytics.css">
	</head>
	
	<body>
		<h1 id="title">Paare finden | Analytics</h1>

<?php
ini_set('display_errors', 1);
error_reporting(-1);

// If there is no game id, we fail
if (!isset($_SERVER['QUERY_STRING'])) die;

// If game id is empty, we fail
if (empty($_SERVER['QUERY_STRING'])) die;

// If there is no game definition for the given game id, we fail
if (!file_exists('games/' . urldecode($_SERVER['QUERY_STRING']) . '.json')) die;

// If there are no logs for the given game id, we fail
if (!file_exists('logs/' . urldecode($_SERVER['QUERY_STRING']))) die;

// Logdateien einlesen
$logFiles = array_diff(scandir('logs/' . urldecode($_SERVER['QUERY_STRING'])), array('..', '.'));

// Kartendaten
$cardStatistics = Array();
$pairStatistics = Array();
$learnedPairs = Array();
$groups = Array();
$graphNodes = Array();
$graphEdges = Array();

// Globale Statistik
$completeGames = 0;
$partialGames = 0;
$allMatchSeen = Array('partial' => 0, 'complete' => 0);
$allMatchUnseen = Array('partial' => 0, 'complete' => 0);
$allPickedWrong = Array('partial' => 0, 'complete' => 0);
$allMissedSeen = Array('partial' => 0, 'complete' => 0);
$allUnseenMisses = Array('partial' => 0, 'complete' => 0);

foreach ($logFiles as $logFile) {
	if (str_starts_with($logFile, 'partial')) {
		$gameType = 'partial';
		$partialGames++;
	} else {
		$gameType = 'complete';
		$completeGames++;
	}
	//echo '<hr />';
	//echo 'Starting to read file ' . $logFile . '<br />';
	$jsonString = file_get_contents('logs/' . urldecode($_SERVER['QUERY_STRING']) . '/' . $logFile);
	$jsonData = json_decode($jsonString, true);
	//var_dump($jsonData);

	$seenElements = Array();
	$firstElement = '';
	$firstContent = '';

	foreach ($jsonData as $logEntry) {
		// Detect new logs with first entry for all elements
		if (!isset($logEntry['time'])) {
			continue;
		}
		
		if (empty($firstElement)) {
			$firstElement = $logEntry['element'];
			$firstContent = $logEntry['content'];
			$firstKey = substr($firstElement, 0, -2);
			$firstStatisticsKey = $firstContent . '-' . $firstKey;
			
			if (!array_key_exists($firstStatisticsKey, $cardStatistics)) {
				$cardStatistics[$firstStatisticsKey] = Array('inGame' => 0, 'matchSeen' => 0, 'matchUnseen' => 0, 'missedSeen' => 0, 'pickedWrong' => 0, 'missedUnseen' => 0, 'mistakes' => Array());
			}
			if (!in_array($firstElement, $seenElements)) {
				$cardStatistics[$firstStatisticsKey]['inGame']++;
				$seenElements[] = $firstElement;
				$graphNodes[] = '"' . addslashes($firstStatisticsKey) . '-' . $firstElement . '" [label="' . addslashes(str_replace('http://commons.wikimedia.org/wiki/Special:FilePath/', '', $firstContent)) . '"]';
			}
			if (!array_key_exists($firstStatisticsKey, $pairStatistics)) {
				$pairStatistics[$firstStatisticsKey] = Array();
				$pairStatistics[$firstStatisticsKey]['unknown'] = Array('missedUnseen' => 0);
			}
		} else {
			$secondElement = $logEntry['element'];
			$secondContent = $logEntry['content'];
			$secondKey = substr($secondElement, 0, -2);
			$secondStatisticsKey = $secondContent . '-' . $secondKey;
			
			if (!array_key_exists($secondStatisticsKey, $cardStatistics)) {
				$cardStatistics[$secondStatisticsKey] = Array('inGame' => 0, 'matchSeen' => 0, 'matchUnseen' => 0, 'missedSeen' => 0, 'pickedWrong' => 0, 'missedUnseen' => 0, 'mistakes' => Array());
			}

			$pairMatch = ($firstKey == $secondKey);
			$pairPartnerSeen = false;
			$pairPartner = '';
			foreach ($seenElements as $seenElement) {
				if ($firstElement != $seenElement && $firstKey == substr($seenElement, 0, -2)) {
					$pairPartnerSeen = true;
					$pairPartner = $seenElement;
					break;
				}
			}
			
			// Paar merken
			if ($pairMatch) {
				if (!array_key_exists($firstKey, $learnedPairs)) $learnedPairs[$firstKey] = Array();
				if (!array_key_exists(substr($firstElement, -1), $learnedPairs[$firstKey])) {
					$learnedPairs[$firstKey][substr($firstElement, -1)] = Array(0 => $firstContent);
					//echo "Learning " . $firstContent . " as first instance of pair " . $pairIndex . " & element " . substr($firstElement, -1) . "<br />";
				} else {
					if (!in_array($firstContent, $learnedPairs[$firstKey][substr($firstElement, -1)])) $learnedPairs[$firstKey][substr($firstElement, -1)][] = $firstContent;
					//echo "Learning " . $firstContent . " as another instance of pair " . $pairIndex . " & element " . substr($firstElement, -1) . "<br />";
				}
				if (!array_key_exists(substr($secondElement, -1), $learnedPairs[$firstKey])) {
					$learnedPairs[$firstKey][substr($secondElement, -1)] = Array(0 => $secondContent);
					//echo "Learning " . $secondContent . " as first instance of pair " . $pairIndex . " & element " . substr($secondElement, -1) . "<br />";
				} else {
					if (!in_array($secondContent, $learnedPairs[$firstKey][substr($secondElement, -1)])) $learnedPairs[$firstKey][substr($secondElement, -1)][] = $secondContent;
					//echo "Learning " . $secondContent . " as another instance of pair " . $pairIndex . " & element " . substr($secondElement, -1) . "<br />";
				}
			}
			
			$edgeKey = '"' . addslashes($firstStatisticsKey) . '-' . $firstElement . '" -> "' . addslashes($secondStatisticsKey) . '-' . $secondElement . '"';
			
			// Paar analysieren
			if ($pairMatch && $pairPartnerSeen) {
				// Match on seen element
				//echo 'Match on seen element: ' . $firstElement . ' -> ' . $secondElement . '<br />';
				$cardStatistics[$firstStatisticsKey]['matchSeen']++;
				$allMatchSeen[$gameType]++;
				if (!array_key_exists($secondElement, $pairStatistics[$firstStatisticsKey])) {
					$pairStatistics[$firstStatisticsKey][$secondElement] = Array('matchSeen' => 0, 'matchUnseen' => 0, 'missedSeen' => 0, 'pickedWrong' => 0);
				}
				$pairStatistics[$firstStatisticsKey][$secondElement]['matchSeen']++;
				
				if (!array_key_exists($edgeKey, $graphEdges)) {
					$graphEdges[$edgeKey] = 1;
				} else {
					$graphEdges[$edgeKey] = $graphEdges[$edgeKey] + 1;
				}
			} else if ($pairMatch && !$pairPartnerSeen) {
				// Match on unseen element
				//echo 'Match on unseen element: ' . $firstElement . ' -> ' . $secondElement . '<br />';
				$cardStatistics[$firstStatisticsKey]['matchUnseen']++;
				$allMatchUnseen[$gameType]++;
				if (!array_key_exists($secondElement, $pairStatistics[$firstStatisticsKey])) {
					$pairStatistics[$firstStatisticsKey][$secondElement] = Array('matchSeen' => 0, 'matchUnseen' => 0, 'missedSeen' => 0, 'pickedWrong' => 0);
				}
				$pairStatistics[$firstStatisticsKey][$secondElement]['matchUnseen']++;
				// TODO: Check if last element
				
			} else if (!$pairMatch && in_array($secondElement, $seenElements)) {
				// Picked a wrong seen element
				//echo 'Picked a wrong seen element: ' . $firstElement . ' -> ' . $secondElement . '<br />';
				$cardStatistics[$firstStatisticsKey]['pickedWrong']++;
				$allPickedWrong[$gameType]++;
				if ($pairPartnerSeen) {
					if (!array_key_exists($pairPartner, $pairStatistics[$firstStatisticsKey])) {
						$pairStatistics[$firstStatisticsKey][$pairPartner] = Array('matchSeen' => 0, 'matchUnseen' => 0, 'missedSeen' => 0, 'pickedWrong' => 0);
					}
					$pairStatistics[$firstStatisticsKey][$pairPartner]['pickedWrong']++;
				} else {
					$pairStatistics[$firstStatisticsKey]['unknown']['missedUnseen']++;
				}

				if (!array_key_exists($edgeKey, $graphEdges)) {
					$graphEdges[$edgeKey] = 1;
				} else {
					$graphEdges[$edgeKey] = $graphEdges[$edgeKey] + 1;
				}
				
				if (!array_key_exists($secondContent, $cardStatistics[$firstStatisticsKey]['mistakes'])) {
					$cardStatistics[$firstStatisticsKey]['mistakes'][$secondContent] = 1;
				} else {
					$cardStatistics[$firstStatisticsKey]['mistakes'][$secondContent] = $cardStatistics[$firstStatisticsKey]['mistakes'][$secondContent] + 1;
				}
			} else if (!$pairMatch && $pairPartnerSeen) {
				// Missed a seen element
				//echo 'Missed a seen element: ' . $firstElement . ' -> ' . $secondElement . '<br />';
				$cardStatistics[$firstStatisticsKey]['missedSeen']++;
				$allMissedSeen[$gameType]++;
				if (!array_key_exists($pairPartner, $pairStatistics[$firstStatisticsKey])) {
					$pairStatistics[$firstStatisticsKey][$pairPartner] = Array('matchSeen' => 0, 'matchUnseen' => 0, 'missedSeen' => 0, 'pickedWrong' => 0);
				}
				$pairStatistics[$firstStatisticsKey][$pairPartner]['missedSeen']++;

				if (!array_key_exists($edgeKey, $graphEdges)) {
					$graphEdges[$edgeKey] = 1;
				} else {
					$graphEdges[$edgeKey] = $graphEdges[$edgeKey] + 1;
				}
			} else if (!$pairMatch && !$pairPartnerSeen) {
				// Missed an unseen element
				//echo 'Missed an unseen element: ' . $firstElement . ' -> ' . $secondElement . '<br />';
				$cardStatistics[$firstStatisticsKey]['missedUnseen']++;
				$allUnseenMisses[$gameType]++;
				//$pairStatistics[$firstElement]['unknown']['missedUnseen']++;
			}			

			if (!in_array($secondElement, $seenElements)) {
				$cardStatistics[$secondStatisticsKey]['inGame']++;
				$seenElements[] = $secondElement;
				$graphNodes[] = '"' . addslashes($secondStatisticsKey) . '-' . $secondElement . '" [label="' . addslashes(str_replace('http://commons.wikimedia.org/wiki/Special:FilePath/', '', $secondContent)) . '"]';
			}

			$firstElement = '';
			$firstContent = '';
			$firstKey = '';
		}
	}
}

//var_dump($learnedPairs);
// Add dummies for unknown cards with index 0
foreach ($learnedPairs as $key => $group) {
	if (!array_key_exists(0, $group)) {
		$learnedPairs[$key][0] = Array(0 => '?');
	}
}

ksort($cardStatistics);

$allMatchSeenTotal = $allMatchSeen['partial'] + $allMatchSeen['complete'];
$allMatchUnseenTotal = $allMatchUnseen['partial'] + $allMatchUnseen['complete'];
$allPickedWrongTotal = $allPickedWrong['partial'] + $allPickedWrong['complete'];
$allMissedSeenTotal = $allMissedSeen['partial'] + $allMissedSeen['complete'];
$allUnseenMissesTotal = $allUnseenMisses['partial'] + $allUnseenMisses['complete'];

$allMatchesTotal = $allMatchSeenTotal + $allMatchUnseenTotal;
$allTrueFailsTotal = $allPickedWrongTotal + $allMissedSeenTotal;
$allChancesTotal = $allMatchesTotal + $allTrueFailsTotal;
$allChancesComplete = $allMatchSeen['complete']+$allMatchUnseen['complete']+$allPickedWrong['complete']+$allMissedSeen['complete'];
$allChancesPartial = $allMatchSeen['partial']+$allMatchUnseen['partial']+$allPickedWrong['partial']+$allMissedSeen['partial'];

$games = $completeGames + $partialGames;
$totalActions = $allMatchesTotal + $allTrueFailsTotal + $allUnseenMissesTotal;
$totalActionsComplete = $allMatchSeen['complete'] + $allMatchUnseen['complete'] + $allPickedWrong['complete'] + $allMissedSeen['complete'] + $allUnseenMisses['complete'];
$totalActionsPartial = $allMatchSeen['partial'] + $allMatchUnseen['partial'] + $allPickedWrong['partial'] + $allMissedSeen['partial'] + $allUnseenMisses['partial'];

$globalErrorRate = $allTrueFailsTotal/$allChancesTotal;
if ($allChancesComplete > 0) {
	$completeErrorRate = ($allPickedWrong['complete']+$allMissedSeen['complete']) / ($allChancesComplete);
} else {
	$completeErrorRate = 0;
}
if ($allChancesPartial > 0) {
	$partialErrorRate = ($allPickedWrong['partial']+$allMissedSeen['partial']) / ($allChancesPartial);
} else {
	$partialErrorRate = 0;
}

echo '<p style="text-align:center">Insgesamt ' . $completeGames . ' vollständige und ' . $partialGames . ' unvollständige Spiele<br />';
if ($completeGames > 0) {
	echo 'Durchschnittlich ' . (round(100*($totalActionsComplete/$completeGames))/100) . ' Aktionen pro Spiel und ' . (round(10000*$completeErrorRate)/100) . '% Fehlerrate in vollständigen Spielen<br />';
}
if ($partialGames > 0) {
	echo 'Durchschnittlich ' . (round(100*($totalActionsPartial/$partialGames))/100) . ' Aktionen pro Spiel und ' . (round(10000*$partialErrorRate)/100) . '% Fehlerrate in unvollständigen Spielen<br />';
}
if ($games > 0) {
	echo 'Insgesamt durchschnittlich ' . (round(100*($totalActions/$games))/100) . ' Aktionen pro Spiel und ' . (round(10000*$globalErrorRate)/100) . '% Fehlerrate in allen Spielen<br />';
}

//echo '<p style="text-align:center">' . $allMatches . ' Treffer in ' . $games . ' Spielen; durchschnittlich ' . (round(100*($totalActions/$games))/100) . ' Aktionen pro Spiel; durchschnittlich ' . (round(10000*$globalErrorRate)/100) . '% Fehlerrate<br />Signifikante Abweichungen der Fehlerraten sind markiert: * = 1%-Niveau, **=0,1%-Niveau</p>';

function nChooseK($n, $k)
{
    $top    = 1;
    $bottom = 1;

    for ($i = 0; $i < $k; $i++) {
        $top    *= $n - $i;
        $bottom *= $i + 1;
    }

    return $top / $bottom;
}

function prob($chances, $fails, $errorRate) {
	return nChooseK($chances, $fails) * pow($errorRate, $fails) * pow(1 - $errorRate, $chances-$fails);
}

// Vorbereitung und Ausgabe der Tabelle
function cmp($a, $b) {
	if ($a[0][0]=='?' && $b[0][0]!='?') return 1;
	if ($a[0][0]!='?' && $b[0][0]=='?') return -1;
	
    return strcmp($a[0][0], $b[0][0]);
}

uasort($learnedPairs, 'cmp');

echo '<table border="0" style="margin:auto">';

foreach($learnedPairs as $key => $group) {
	ksort($group);
	echo '<tr><td>';
	$matchSeen = 0;
	$matchUnseen = 0;
	$missedSeen = 0;
	$pickedWrong = 0;
	$trueFails = 0;
	$missedUnseen = 0;
	
	$lastMemberKey = 0;
	
	$expandList = Array();
	
	foreach($group as $memberKey => $members) {
		if ($memberKey > $lastMemberKey+1) {
			echo '<div class="card-block"></div>';
		}
		$lastMemberKey = $memberKey;
		echo '<div class="card-block">';
		
		foreach($members as $member) {
			if ($member == '?') {
				echo '<div class="card-data"><div class="game-card"><div class="back-face">?</div></div><div class="detail-card">Bisher nicht im Spiel</div></div>';
			} else {
				echo '<div class="card-data"><div class="game-card"><div class="front-face">' . (stripos($member, '.jpg', 1)>1||stripos($member, '.png', 1)>1||stripos($member, '.svg', 1)>1?'<img src="'.$member.'" />':$member) . '</div></div>';
				
				$cardKey = $member . '-' . $key;
				$expandList[] = addslashes($cardKey);
				if (!array_key_exists($cardKey, $cardStatistics)) {
					$cardStatistics[$cardKey] = Array('inGame' => 0, 'matchSeen' => 0, 'matchUnseen' => 0, 'missedSeen' => 0, 'pickedWrong' => 0, 'missedUnseen' => 0, 'mistakes' => Array());
				}
				$errorRate = 0;
				$significance = '';
				if ($cardStatistics[$cardKey]['matchSeen'] + $cardStatistics[$cardKey]['missedSeen'] + $cardStatistics[$cardKey]['pickedWrong'] > 0) {
					$fails = $cardStatistics[$cardKey]['missedSeen'] + $cardStatistics[$cardKey]['pickedWrong'];
					$chances = $cardStatistics[$cardKey]['matchSeen'] + $cardStatistics[$cardKey]['missedSeen'] + $cardStatistics[$cardKey]['pickedWrong'];
					$errorRate = (round(10000*$fails/$chances)/100);
					$probability = prob($chances, $fails, $globalErrorRate);
					if ($probability < 0.001) {
						$significance = '**';
					} else if ($probability < 0.01) {
						$significance = '*';
					}
				}
				
				echo '<div class="detail-card"><span style="font-size:120%;font-weight:bold">' . $cardStatistics[$cardKey]['inGame'] . ' mal im Spiel</span><br /><span style="color:green">' . $cardStatistics[$cardKey]['matchSeen'] . ' Chance genutzt</span><br />' . $cardStatistics[$cardKey]['matchUnseen'] . ' Zufallstreffer<br />' . $cardStatistics[$cardKey]['missedUnseen'] . ' keine Chance<br /><span style="color:red">' . $cardStatistics[$cardKey]['missedSeen'] . ' Chance vertan<br />' . $cardStatistics[$cardKey]['pickedWrong'] . ' Fehlgriff</span><br /><span style="font-size:120%;font-weight:bold">Fehlerrate ' . $errorRate . '%' . $significance . '</span></div><br /><div id="' . addslashes($cardKey) . '" class="mistakes-list" style="height:170px;"><b>Häufigste Fehlgriffe:</b><br />';
				
				arsort($cardStatistics[$cardKey]['mistakes']);
				
				$mistakes = 0;
				foreach($cardStatistics[$cardKey]['mistakes'] as $content => $count) {
					echo $count . 'x <div class="game-card"><div class="front-face">' . (stripos($content, '.jpg', 1)>1||stripos($content, '.png', 1)>1||stripos($content, '.svg', 1)>1?'<img src="'.$content.'" />':$content) . '</div></div>';
					if (++$mistakes >= 3) break;
				}
				if ($mistakes == 0) {
					echo '<i>bisher keine Daten</i>';
				}
				
				echo '<br /><b>Fehler-Paar-Analyse:</b><br />';
				$partnerNames = Array('A', 'B', 'C');
				$anyOutput = false;
				foreach($pairStatistics[$cardKey] as $partner => $partnerData) {
					if ($partner == 'unknown') {
						if ($partnerData['missedUnseen'] > 0) {
							echo $partnerData['missedUnseen'] . 'x Partner unbekannt<br />';
							$anyOutput = true;
						}
					} else {
						$index = substr($partner, -1);
						$errorRate = 0;
						$significance = '';
						$fails = $partnerData['missedSeen'] + $partnerData['pickedWrong'];
						$chances = $partnerData['missedSeen'] + $partnerData['pickedWrong'] + $partnerData['matchSeen'];
						if ($chances > 0) {
							$errorRate = (round(10000*$fails/$chances)/100);
							$probability = prob($chances, $fails, $globalErrorRate);
							if ($probability < 0.001) {
								$significance = '**';
							} else if ($probability < 0.01) {
								$significance = '*';
							}
						}
						
						echo $partnerNames[$index] . ': ' . $fails . '/' . $chances . ' = ' . $errorRate . '%' . $significance . '<br />';
						$anyOutput = true;
					}
				}
				if (!$anyOutput) {
					echo '<i>bisher keine Daten</i>';
				}
				
				echo '</div></div>';
				
				$matchSeen += $cardStatistics[$cardKey]['matchSeen'];
				$matchUnseen += $cardStatistics[$cardKey]['matchUnseen'];
				$missedSeen += $cardStatistics[$cardKey]['missedSeen'];
				$pickedWrong += $cardStatistics[$cardKey]['pickedWrong'];
				$trueFails += ($cardStatistics[$cardKey]['missedSeen'] + $cardStatistics[$cardKey]['pickedWrong']);
				$missedUnseen += $cardStatistics[$cardKey]['missedUnseen'];
			}
		}
		
		echo '</div>';
	}
	$total = $matchSeen + $matchUnseen;
	$totalChances = $matchSeen + $missedSeen + $pickedWrong;
	$errorRate = 0;
	if ($totalChances > 0) {
		$errorRate = round(10000*$trueFails/$totalChances)/100;
	}
	$significance = '';
	$probability = prob($totalChances, $trueFails, $globalErrorRate);
	if ($probability < 0.001) {
		$significance = '**';
	} else if ($probability < 0.01) {
		$significance = '*';
	}
	
	echo '</td><td style="width:220px;" onclick="';
	foreach($expandList as $expandKey) {
		echo 'document.getElementById(\'' . $expandKey . '\').style.maxHeight==\'170px\'?document.getElementById(\'' . $expandKey . '\').style.maxHeight=\'0px\':document.getElementById(\'' . $expandKey . '\').style.maxHeight=\'170px\';';
	}
	echo '"><span style="font-size:70%">Gruppen-ID: ' . $key . '</span><br /><span style="font-size:130%;font-weight:bold">' . $total . ' mal im Spiel</span><br /><span style="color:green">' . $matchSeen . ' Chance genutzt</span><br />' . $matchUnseen . ' Zufallstreffer<br />' . $missedUnseen . ' keine Chance<br /><span style="color:red">' . $missedSeen . ' Chance vertan<br />' . $pickedWrong . ' Fehlgriff</span><br /><span style="font-size:130%;font-weight:bold">Fehlerrate ' . $errorRate . '%' . $significance . '</span></td></tr>';
}


?>
			<tr><td colspan="2"><p style="text-align:left">Legende:<br /><span style="color:green">Chance genutzt</span> = die zugehörige Karte war bekannt und wurde auch gewählt<br />Zufallstreffer = die zugehörige Karte war nicht bekannt und wurde zufälligerweise gewählt<br />keine Chance = die zugehörige Karte war nicht bekannt und es wurde eine andere, unbekannte Karte gewählt<br /><span style="color:red">Chance vertan</span> = die zugehörige Karte war bekannt, aber es wurde eine andere, unbekannte Karte gewählt<br /><span style="color:red">Fehlgriff</span> = die zugehörige Karte war bekannt oder nicht bekannt, aber es wurde eine andere, bekannte Karte gewählt</p></td></tr>
		</table>
		
	</body>
</html>